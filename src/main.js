import Vue from 'vue';
import App from './App.vue';
import './registerServiceWorker';
import router from './router';
import store from './store';
import vuetify from './plugins/vuetify';

Vue.config.productionTip = false;

const recaptchaScript = document.createElement('script');
recaptchaScript.setAttribute('src', `https://www.google.com/recaptcha/api.js?render=${process.env.VUE_APP_RECHAPTA_SITE_KEY}`);
document.head.appendChild(recaptchaScript);
new Vue({
  router,
  store,
  vuetify,
  render(h) { return h(App); },
}).$mount('#app');
