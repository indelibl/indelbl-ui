import Vue from 'vue';
import Vuex from 'vuex';
import PostWithChapta from './lib/postWithChapta';
import router from '@/router';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    url: null,
  },
  mutations: {
    setUrl(state, url) {
      state.url = url;
      router.push(`/r/${url}`);
    },
  },
  actions: {
    enterUrl(context, obj) {
      PostWithChapta('http://localhost:8080/r', {
        Url: obj.url,
      })
        .then((url) => {
          context.commit('setUrl', `${url.Host}${url.Path}/${url.id}`);
        });
    },
  },
  modules: {
  },
});
