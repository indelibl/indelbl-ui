// eslint-disable-next-line no-undef
import axios from 'axios';

// eslint-disable-next-line no-undef
export default (url, body) =>
  // eslint-disable-next-line no-undef,implicit-arrow-linebreak
  grecaptcha.execute(process.env.VUE_APP_RECHAPTA_SITE_KEY, { action: 'scrape' })
    .then((token) => axios.post(url, body, {
      headers: {
        grctoken: token,
      },
    }));
